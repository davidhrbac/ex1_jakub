ALL: build

SRC = ex1.c

-include ${PETSC_DIR}/lib/petsc/conf/variables

MAINFILE := ex1
CPPFLAGS := -I./include ${PETSC_CCPPFLAGS}
CFLAGS   := ${CC_FLAGS}
OBJ      := $(SRC:.c=.o)
LIB      := ${PETSC_TAO_LIB}


src/%.o: %.c
	@echo ${PETSC_COMPILE_SINGLE}

${MAINFILE}: ${OBJ}
	-${CLINKER} -o ${MAINFILE} ${OBJ} ${LIB} 

build: ${MAINFILE}

clean:
	-@${RM} ${OBJ}
	-@${RM} ${OBJ:.o=.d}
	-@${RM} ${MAINFILE}

.PHONY: ALL build clean

